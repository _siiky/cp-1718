BASENAME := cp1718t

all: $(BASENAME).pdf $(BASENAME).bib

$(BASENAME).tex: $(BASENAME).lhs
	lhs2TeX $< > $@

$(BASENAME).pdf: $(BASENAME).tex
	pdflatex $<

$(BASENAME).idx: $(BASENAME).tex
	makeindex $@

$(BASENAME).bib:
	bibtex $(BASENAME)

# Automatically compile what is necessary, as many times as needed.
rubber:
	@rubber --pdf -f $(BASENAME)

clean:
	$(RM) -r *.aux *.log *.bbl *.bak *.ptb *.blg *.out *.spl *.idx

cleanall: clean
	$(RM) $(BASENAME).pdf

.PHONY: rubber all rubber clean cleanall
